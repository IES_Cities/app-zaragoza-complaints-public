//http://www.zaragoza.es/ciudad/risp/open311.htm#Listadodequejas
'use strict';

/** Cordova ready listener */
iescities.factory('cordovaReady', function() {
	return function(fn) {

		var queue = [];

		var impl = function() {
			queue.push(Array.prototype.slice.call(arguments));
		};

		document.addEventListener('deviceready', function() {
			queue.forEach(function(args) {
				fn.apply(this, args);
			});
			impl = fn;
		}, false);

		return function() {
			return impl.apply(this, arguments);
		};
	};
});

/** Geolocation service */
iescities.factory('geolocation', [
		'$rootScope',
		'cordovaReady',
		function($rootScope, cordovaReady) {
			return {
				getCurrentPosition : function(onSuccess, onError, options) {
					var success = function() {
						var that = this, args = arguments;

						if (onSuccess) {
							$rootScope.$apply(function() {
								onSuccess.apply(that, args);
							});
						}
					}, error = function() {
						var that = this, args = arguments;

						if (onError) {
							$rootScope.$apply(function() {
								onError.apply(that, args);
							});
						}
					};
					if (document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1
							&& document.URL.indexOf('file://') === -1) {
						// PhoneGap application
						cordovaReady(function(onSuccess, onError, options) {
							navigator.geolocation.getCurrentPosition(success, error, options);
						});
					} else if (navigator.geolocation) {
						// Web page
						navigator.geolocation.getCurrentPosition(success, error, options);
					}
				}
			};
		} ]);

/** Services info service */
iescities.factory('servicesInfoService', [ '$http', function($http) {
	return {

		getServices : function(callback) {
			return $http.get('http://www.zaragoza.es/api/recurso/open311/services.json').success(callback);
		}

	};
} ]);

/** Profile info service */
iescities.factory('profileInfoService', [
		'$http',
		'$rootScope',
		function($http, $rootScope) {
			return {

				// Get profile info
				getProfileInfo : function(callback) {
					return $http.get('scripts/iescities/data/getProfileInfo.json').success(callback);
				},

				// Save profile info
				saveProfileInfo : function(profile, callback) {
					var json_data = JSON.stringify(profile);
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "POST" + "/api/recurso/users/open311/new" + json_data,
							"UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'POST',
						url : 'http://www.zaragoza.es/api/recurso/users/open311/new',
						data : json_data,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8',
							'User-Agent' : 'iescities[movil]',
							'Referer' : 'http://www.zaragoza.es/ciudad/'
						}
					}).success(function(data) {
						$rootScope.account_id = data.account_id;
						$rootScope.profileData = data;
						Restlogging.appLog(logType.PROSUME,"new user created");	
					}).error(function(data) {
						// console.log(JSON.stringify(data));
					});

				},

				// Update profile info
				updateProfileInfo : function(profile, callback) {
					var json_data = JSON.stringify(profile);
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "PUT" + "/api/recurso/users/open311/" + $rootScope.account_id
							+ json_data, "UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'PUT',
						url : 'http://www.zaragoza.es/api/recurso/users/open311/' + $rootScope.account_id,
						data : json_data,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8',
							'User-Agent' : 'iescities[movil]',
							'Referer' : 'http://www.zaragoza.es/ciudad/'
						}
					}).success(function(data, status, header, config) {
						$rootScope.account_id = data.account_id;
						$rootScope.profileData = data;
					}).error(function(status) {
						// console.log("Status:" + status.error);
					});
				}

			};
		} ]);

/** Complaints service */
iescities.factory('complaintsService', [
		'$http',
		'$rootScope',		
		function($http, $rootScope) {
			return {

				// Saves new complaint
				saveNewComplaint : function(queja) {
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "POST" + "/api/recurso/open311/request" + queja,
							"UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'POST',
						url : 'http://www.zaragoza.es/api/recurso/open311/request',
						data : queja,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						}
					}).success(function(data) {
						Restlogging.appLog(logType.COLLABORATE,"new complaint added");						
						// console.log(JSON.stringify(data));
					}).error(function(status) {
						// console.log("Status:" + status.error);
					});
				},

				// Gets last complaints
				getComplaints : function(callback) {
					return $http.get('http://www.zaragoza.es/api/recurso/open311/requests.json?rows=100').success(callback);
				},

				// Gets dummy complaints from local
				getDummyComplaints : function(callback) {
					return $http.get('scripts/iescities/data/getRequests.json').success(callback);
				},

				// Get user complaints
				getMyComplaints : function() {
					var uri = "http://www.zaragoza.es/api/recurso/open311/requests?account_id=" + $rootScope.account_id;
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "GET" + "/api/recurso/open311/requests?account_id="
							+ $rootScope.account_id, "UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					$http({
						method : 'GET',
						url : uri,
						headers : {
							'clientId' : 'eurohelp',
							data : 'account_id=' + $rootScope.account_id,
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						}
					}).success(function(data) {
						//console.log(JSON.stringify(data));
						$rootScope.mycomplaints = data;
						// toastr.info("Quejas obtenidas correctamente");
					}).error(function(data) {
						// console.log(JSON.stringify(data));
						// toastr.info("Ha ocurrido un error");
					});
				}

			};
		} ]);

/** Login service */
iescities.factory('loginService', [
		'$http',
		'$rootScope',		
		function($http, $rootScope) {
			return {

				// Attempts login
				login : function(userData, callback) {
					var login = "email=" + userData.email + "&password=" + userData.password;
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "POST" + "/api/recurso/users/open311/" + login,
							"UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'POST',
						url : 'http://www.zaragoza.es/api/recurso/users/open311/',
						data : login,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						}
					}).success(function(data) {
						// console.log(JSON.stringify(data));
						$rootScope.account_id = data.account_id;
						$rootScope.profileData = data;
						$rootScope.profileData.password = userData.password;
						Restlogging.appLog(logType.CONSUME, "User logged");					
					}).error(function(data) {
						// console.log(JSON.stringify(data));
					});
				}

			};
		} ]);

/** IesCities service */
iescities.factory('iesCitiesService', [ '$http', '$rootScope', function($http, $rootScope) {
	return {

		// Iescities server app log
		sendLog : function(message, type) {
			var uri = "http://150.241.239.65:8080/IESCities/api/log/app/stamp/";
			var timestamp = Math.floor(new Date().getTime() / 1000);
			var appid = "zgzcomplaints";
			var session = $rootScope.sessionId;

			uri = uri + timestamp + "/" + appid + "/" + session + "/" + type + "/" + message;
			console.log(uri);
			return $http({
				method : 'GET',
				url : uri,
				headers : {
					'Accept' : 'application/json',
					'Content-Type' : 'application/json;charset=UTF-8'
				}
			}).success(function(data) {
				console.log("iesCitiesService.sendLog() - " + JSON.stringify(data));
			}).error(function(data) {
				console.log("iesCitiesService.sendLog() - " + JSON.stringify(data));
			});
		}

	};
} ]);

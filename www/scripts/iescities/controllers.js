'use strict';

/** Main controller */
iescities
		.controller(
				'MainCtrl',
				[
						'$rootScope',
						'$scope',
						'geolocation',
						function($rootScope, $scope, geolocation) {

							// Geolocation button
							$scope.geolocateMe = function() {
								document.getElementById("my_location_btn").className = "my_location_btn_spinner ng-scope";
								document.getElementById("my_location_btn").disabled = true;
								geolocation
										.getCurrentPosition(
												function(position) {
													$scope
															.centerMapInPosition(position);
												},
												function(error) {													
													toastr
															.error('No ha sido posible geoposicionar su ubicación.');
													document
															.getElementById("my_location_btn").className = "my_location_btn_normal ng-scope";
													document
															.getElementById("my_location_btn").disabled = false;
												}, {
													maximumAge : 3000,
													timeout : 10000,
													enableHighAccuracy : false
												});
							};

						} ]);

/** Menu controller */
iescities.controller('menuCtrl', [ '$rootScope', '$scope', 'complaintsService',
		function($rootScope, $scope, complaintsService) {

			// Open new complaint Panel
			$scope.openNewComplaintPanel = function() {
				$scope.showPanel('new_complaint');
			};

			// Open public complaints Panel
			$scope.showPublicComplaintsPanel = function() {
				$scope.showPanel('publicComplaints');
			};

			// Open user complaints Panel
			$scope.showUserComplaintsPanel = function() {
				$scope.showPanel('userComplaints');
				complaintsService.getMyComplaints();
			};

			// Open new user Panel
			$scope.showNewUserPanel = function() {
				$scope.showPanel('new_user');
			};

			// Open user profile Panel
			$scope.showUserProfilePanel = function() {
				$scope.showPanel('my_profile');
			};

			// Open Login Panel
			$scope.showLoginPanel = function() {
				$scope.showPanel('login');
			};

		} ]);

/** Complaint location picker controller */
iescities
		.controller(
				'locationPickerCtrl',
				[
						'$rootScope',
						'$scope',
						function($rootScope, $scope) {

							// Adds picked location to complaint
							$scope.addLocationToComplaint = function() {
								var complaintLocation = $scope.getMapCenter();
								$rootScope.complaintLocation = complaintLocation;
								$rootScope.isLocationPickerMode = false;
								$scope.terminateLocationPicking();
							};

							// Ends location picking mode
							$scope.cancelLocation = function() {
								$rootScope.isLocationPickerMode = false;
								$scope.terminateLocationPicking();
							};

							// Shows new_complaint panel again
							$scope.terminateLocationPicking = function() {
								var panelId = 'new_complaint';
								if (isWebKit()) {
									document.getElementById(panelId).style.webkitTransform = "translate3d(0,0,0)";
								} else {
									document.getElementById(panelId).style.transform = "translate3d(0,0,0)";
								}
								document.getElementById(panelId).style.zIndex = 200;
							};

						} ]);

/** New Complaint controller */
iescities
		.controller(
				'newComplaintCtrl',
				[
						'$window',
						'$rootScope',
						'$scope',
						'$http',
						'$timeout',
						'$upload',
						'complaintsService',
						'ngProgress',
						function($window, $rootScope, $scope, $http, $timeout,
								$upload, complaintsService, ngProgress) {

							$scope.publicComplaint = false;

							$scope.sendingComplaint = false;

							// if($scope.account_id != null){
							// complaintsService.getMyComplaints();
							// }

							$scope.fileReaderSupported = window.FileReader != null;

							$scope.fileReader = new FileReader();

							$scope.hasUploader = function(index) {
								return $scope.upload[index] != null;
							};
							$scope.abort = function(index) {
								$scope.upload[index].abort();
								$scope.upload[index] = null;
							};

							$scope.onFileSelect = function($files) {
								$scope.selectedFiles = [];
								$scope.selectedFileName = [];
								// $scope.progress = [];
								if ($scope.upload && $scope.upload.length > 0) {
									for (var i = 0; i < $scope.upload.length; i++) {
										if ($scope.upload[i] != null) {
											$scope.upload[i].abort();
										}
									}
								}
								$scope.upload = [];
								$scope.uploadResult = [];
								$scope.selectedFiles = $files;
								$scope.dataUrls = [];
								for (var i = 0; i < $files.length; i++) {
									var $file = $files[i];
									if (window.FileReader
											&& $file.type.indexOf('image') > -1) {
										// var fileReader = new FileReader();
										$scope.fileReader = new FileReader();
										$scope.fileReader
												.readAsDataURL($files[i]);
										$scope.selectedFileName[i] = $file.name;
										setPreview($scope.fileReader, i);
									}
									// $scope.progress[i] = -1;

									$scope.start(i);

								}
							}

							function setPreview(fileReader, index) {
								fileReader.onload = function(e) {
									$timeout(function() {
										$scope.dataUrls[index] = e.target.result;
									});
								}
							}

							$scope.start = function(index) {
								// $scope.progress[index] = 0;
								var fileReader = new FileReader();
								fileReader
										.readAsArrayBuffer($scope.selectedFiles[index]);
							}

							// Save complaint
							$scope.saveComplaint = function() {

								if ($scope.sendingComplaint) {
									toastr
											.info("La queja está siendo enviada...");
								} else {

									if (this.complaintForm.$valid) {

										var filename = "";
										var filebody = [];
										var queja = "";
										if ($scope.account_id == null) {
											// Queja anónima
											queja = "address_string="
													+ $scope.newComplaintData.user_address
													+ "&email="
													+ $scope.newComplaintData.email
													+ "&device_id=tt222111&account_id="
													+ ""
													+ "&first_name="
													+ $scope.newComplaintData.first_name
													+ "&last_name="
													+ $scope.newComplaintData.last_name
													+ "&phone="
													+ $scope.newComplaintData.phone
													+ "&title="
													+ $scope.newComplaintData.title
													+ "&description="
													+ $scope.newComplaintData.descripcion;
										} else {
											// Queja de usuario registrado
											queja = "address_string="
													+ $scope.profileData.user_address
													+ "&email="
													+ $scope.profileData.email
													+ "&device_id=tt222111&account_id="
													+ $scope.account_id
													+ "&first_name="
													+ $scope.profileData.first_name
													+ "&last_name="
													+ $scope.profileData.last_name
													+ "&phone="
													+ $scope.profileData.phone
													+ "&title="
													+ $scope.newComplaintData.title
													+ "&description="
													+ $scope.newComplaintData.descripcion;
											// +
											// "TEST-IESCITIES-XXX&description=TEST-IESCITIES-XXX";
										}

										// add location if picked
										if ($rootScope.complaintLocation != null) {
											var location = $rootScope.complaintLocation;
											queja = queja + "&lat="
													+ location.lat + "&lon="
													+ location.lon;
										}

										if ($scope.publicComplaint) {
											// TODO: nombre del parámetro a
											// pasar?
											queja = queja + "&public=S";
											// queja = queja +
											// "&attribute[public]=S";
										}

										queja = queja.replace(/undefined/gi,
												' ');
										
										// add photo if picked
										if ($rootScope.newComplaintPhoto != null) {
											filename = new Date().getTime()
													+ ".jpeg";
											filebody = $rootScope.newComplaintPhoto;
											filebody = filebody.replace(
													"data:image/jpeg;base64,",
													"");
											filebody = encodeURIComponent(filebody);
											console.log(filename);
											console.log(filebody);
											queja = queja + "&media_name="
													+ filename + "&media_body="
													+ filebody;
											console.log(queja.length);
										}

										// if (this.fileReader.readyState == 2)
										// {
										// filebody = $scope.dataUrls[0];
										// filename =
										// $scope.selectedFileName[0];
										// queja = queja + "&media_name=" +
										// filename
										// + "&media_body=" + filebody;
										// }

										console.log(JSON.stringify(queja));
										ngProgress.height('4px');
										ngProgress.color('green');
										ngProgress.start();
										$scope.sendingComplaint = true;
										complaintsService
												.saveNewComplaint(queja)
												.success(
														function(data) {
															ngProgress
																	.complete();
															$scope.sendingComplaint = false;
															toastr
																	.info("La queja se ha registrado correctamente");
															$scope.newComplaintData = null;
															$scope.publicComplaint = false;
															$rootScope.newComplaintPhoto = null;
															$rootScope.complaintLocation = null;
															$scope
																	.closePanel('new_complaint');
															$rootScope.mycomplaints
																	.push(data);
														})
												.error(
														function(data) {
															ngProgress
																	.complete();
															$scope.sendingComplaint = false;
															// console.log(JSON.stringify(data));
															toastr
																	.info("Ha ocurrido un error");
														});

									} else {
										toastr
												.info("El título y la descripción son obligatorios");
									}

								}

							};

							$scope.addPictureToComplaint = function() {
								$scope.addMediaToComplaint(
										Camera.PictureSourceType.CAMERA,
										Camera.DestinationType.DATA_URI);
							};

							$scope.addMediaFromGalleryToComplaint = function() {
								$scope.addMediaToComplaint(
										Camera.PictureSourceType.PHOTOLIBRARY,
										Camera.DestinationType.FILE_URI);
							};

							// Show picked photo
							$scope.addMediaToComplaint = function(mSourceType,
									mDestinationType) {
								$rootScope.newComplaintPhoto = null;
								// TODO: check cordova rdy
								navigator.camera.getPicture(onSuccess, onFail,
										{
											quality : 50,
											// encodingType:
											// Camera.EncodingType.JPEG,
											// destinationType :
											// Camera.DestinationType.DATA_URL
											// -> BASE64 img
											sourceType : mSourceType,
											destinationType : mDestinationType
										// destinationType :
										// Camera.DestinationType.DATA_URI
										});

								function onSuccess(imageData) {
									// http://joeriks.com/2014/04/16/select-images-from-photo-gallery-in-a-cordova-phonegap-app-on-android/
									// Android 4.4 quirk
									if (imageData.substring(0, 21) == "content://com.android") {
										var photo_split = imageData
												.split("%3A");
										imageData = "content://media/external/images/media/"
												+ photo_split[1];
									}
									// Without angular
									// var image =
									// document.getElementById('newphoto');
									// image.src = "data:image/jpeg;base64," +
									// imageData;
									// $scope.$apply(function() {
									// $rootScope.newComplaintPhoto = imageData;
									// });

									// Get file from uri
									var gotFileEntry = function(fileEntry) {
										console.log("got image file entry: "
												+ fileEntry.fullPath);
										fileEntry
												.file(
														function(file) {
															var reader = new FileReader();
															reader.onloadend = function(
																	evt) {
																console
																		.log("Read complete!");
																$scope
																		.$apply(function() {
																			// image64.value
																			// =
																			// evt.target.result;
																			$rootScope.newComplaintPhoto = evt.target.result;
																		});

																// Para guardar
																// en un fichero
																// y comprobar
																// el resultado
																// de la camara
																// function
																// gotFS(fileSystem)
																// {
																// fileSystem.root.getFile("readme.txt",
																// {
																// create :
																// true,
																// exclusive :
																// false
																// },
																// gotFileEntryForWritting,
																// onFail);
																// }
																//
																// function
																// gotFileEntryForWritting(fileEntry)
																// {
																// fileEntry.createWriter(gotFileWriter,
																// onFail);
																// }
																//
																// function
																// gotFileWriter(writer)
																// {
																// writer.onwriteend
																// =
																// function(evt)
																// {
																// console.log("FILE
																// CREATED");
																// };
																// writer.write(evt.target.result);
																// }
																// window.requestFileSystem(LocalFileSystem.PERSISTENT,
																// 0, gotFS,
																// onFail);

															};
															reader
																	.readAsDataURL(file);
														}, onFail);
									};

									window.resolveLocalFileSystemURI(imageData,
											gotFileEntry, onFail);
								}

								function onFail(message) {
									// No hacer nada
								}
							};

							// Closes panel
							$scope.closePanelWithId = function(panelId) {
								$scope.closePanel(panelId);
							};

							// Adds location to current complaint
							$scope.addLocationToComplaint = function() {
								$rootScope.complaintLocation = null;
								$rootScope.isLocationPickerMode = true;
								$scope.closePanel('new_complaint');
							};

						} ]);

/** Complaints list controller */
iescities.controller('complaintsListCtrl', function($scope) {

	// Shows complaint detail
	$scope.showComplaintDetail = function(complaint) {
		if ($scope.isComplaintOpen(complaint)) {
			$scope.opened = undefined;
		} else {
			$scope.opened = complaint;
		}
	};

	// Checks if complaint detail is opened
	$scope.isComplaintOpen = function(complaint) {
		return $scope.opened === complaint;
	};

	// Close panel
	$scope.closePanelWithId = function(panelId) {
		$scope.closePanel(panelId);
	};

});

/** Profile controller */
iescities
		.controller(
				'profileCtrl',
				[
						'$rootScope',
						'$scope',
						'$http',
						'profileInfoService',
						function($rootScope, $scope, $http, profileInfoService) {

							// Save profile data
							$scope.saveProfile = function() {
								
								if ($scope.profileData == null) {
									toastr
											.info("El correo y el password son obligatorios");
									return;
								}

								if ($scope.profileData.first_name == null) {
									toastr
											.info("El nombre es obligatorio");
									return;
								}
								
								
								if ($scope.profileData.email == null) {
									toastr
											.info("No has introducido el correo electrónico");
									return;
								}

								if ($scope.profileData.password == null) {
									toastr
											.info("No has introducido el password");
									return;
								}
								
								if ($scope.profileData.passwordConfirm == null) {
									toastr
											.info("No has confirmado el password");
									return;
								}
								
								if ($scope.profileData.password != $scope.profileData.passwordConfirm){
									toastr
										.info("El password no coincide");
									return;
								}

								var profileBody = {
									"first_name" : $scope.profileData.first_name,
									"last_name" : $scope.profileData.last_name,
									"phone" : $scope.profileData.phone,
									"email" : $scope.profileData.email,
									"password" : $scope.profileData.password,
									"user_address" : $scope.profileData.user_address,
									"city" : $scope.profileData.city,
									"country" : $scope.profileData.country,
									"zipcode" : $scope.profileData.zipcode,
									"nif" : $scope.profileData.nif

								};

								profileInfoService
										.saveProfileInfo(profileBody)
										.success(
												function(data) {
													toastr
															.info("Se ha registrado correctamente");
													$scope
															.closePanel('new_user');
												})
										.error(
												function(data) {
													toastr
															.info("Error al crear usuario: "
																	+ data.mensaje);
												});

							};

							// Update profile data
							$scope.updateProfile = function() {
								if (this.profileForm.$valid) {
									var profileBody = {
										"first_name" : $scope.profileData.first_name,
										"last_name" : $scope.profileData.last_name,
										"phone" : $scope.profileData.phone,
										"email" : $scope.profileData.email,
										"password" : $scope.profileData.password,
										"user_address" : $scope.profileData.user_address,
										"city" : $scope.profileData.city,
										"country" : $scope.profileData.country,
										"zipcode" : $scope.profileData.zipcode,
										"nif" : $scope.profileData.nif

									};

									if ($scope.profileData.currentPassword != $scope.profileData.password) {
										toastr
												.info("El password actual no es válido");
										return;
									}

									if ($scope.profileData.newPassword != undefined
											&& $scope.profileData.newPassword.length > 0) {
										profileBody.password = $scope.profileData.newPassword;
										$scope.profileData.password = $scope.profileData.newPassword;
									}

									profileInfoService
											.updateProfileInfo(profileBody)
											.success(
													function(data) {
														toastr
																.info("Sus datos de perfil se han modificado correctamente");
														$scope
																.closePanel('my_profile');
													})
											.error(
													function(data) {
														toastr
																.info("Error al actualizar: "
																		+ data.mensaje);
													});
								}
							};

							// Close panel
							$scope.closePanelWithId = function(panelId) {
								$scope.closePanel(panelId);
							};

						} ]);

/** Login controller */
iescities.controller('loginCtrl',
		function($scope, loginService, $http, complaintsService) {

			$scope.rememberLoginData = window.localStorage
					.getItem("rememberLoginData") == 'true' ? true : false;

			// Load stored account credential values
			$scope.loginData = {
				email : window.localStorage.getItem("email"),
				password : window.localStorage.getItem("password")
			};

			// Attemps login
			$scope.userLogin = function() {

				if ($scope.loginData == null) {
					toastr.info("Email o contraseña no válido");
					return;
				}

				loginService.login($scope.loginData).success(
						function(data) {
							console.log("logged succesfully...");
							window.localStorage.setItem("rememberLoginData",
									$scope.rememberLoginData);
							if ($scope.rememberLoginData) {
								window.localStorage.setItem("email",
										$scope.loginData.email);
								window.localStorage.setItem("password",
										$scope.loginData.password);
							} else {
								window.localStorage.setItem("email", '');
								window.localStorage.setItem("password", '');
							}
							complaintsService.getMyComplaints();
							$scope.closePanel('login');
						}).error(function(data) {
					toastr.info("Email o contraseña no válido");
				});

			};

			// Close panel
			$scope.closePanelWithId = function(panelId) {
				$scope.closePanel(panelId);
			};

		});

/** privacyCtrl */
iescities.controller('privacyCtrl', [
		'$rootScope',
		'$scope',
		function($rootScope, $scope) {

			$rootScope.privacyVisible = false;

			var isPrivacyAccepted = window.localStorage
					.getItem("isPrivacyAccepted");

			if (isPrivacyAccepted) {
				$rootScope.privacyVisible = false;
			} else {
				$rootScope.privacyVisible = true;
			}

			$scope.acceptPrivacy = function() {
				$rootScope.privacyVisible = false;
				window.localStorage.setItem("isPrivacyAccepted", true);
			};

			$scope.refusePrivacy = function() {
				navigator.app.exitApp();
			};

		} ]);

/** complaintInfoCtrl */
iescities.controller('complaintInfoCtrl', [ '$rootScope', '$scope',
		function($rootScope, $scope) {

			$rootScope.selectedComplaint = null;

			$scope.closeMarkerInfo = function() {
				$rootScope.selectedComplaint = null;
			};

		} ]);

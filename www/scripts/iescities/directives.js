// Directives

/** Zaragoza map directive */
iescities
		.directive(
				'zaragozaMap',
				[
						'$window',
						'$rootScope',
						'$parse',
						'complaintsService',
						function($window, $rootScope, $parse, complaintsService) {

							return {
								restrict : 'A',
								replace : false,
								template : '<div id="map_canvas" style="height: 600px;"></div>',
								link : function($scope, element, attrs) {

									// TOOD: IesCities Server session ID
									var max = 9999999;
									var min = 0;
									$rootScope.sessionId = Math.floor(Math
											.random()
											* (max - min + 1) + min);

									var mapDiv = document
											.getElementById("map_canvas"), bounds, map;

									$(mapDiv).width($(window).width());
									$(mapDiv).height($(window).height());
									OpenLayers.ProxyHost = "proxy.cgi?url=";

									Proj4js.defs["EPSG:23030"] = "+proj=utm +zone=30 +ellps=intl +towgs84=-131,-100.3,-163.4,-1.244,-0.020,-1.144,9.39 +units=m +no_defs";

									var resolutionList = [ 53.125201382285255,
											26.562600691142627,
											14.062553307075499,
											6.718775468936064,
											3.7500142152201517,
											1.7187565153092277,
											0.9375035538050343,
											0.5000018953626857,
											0.26375099980381617 ];

									map = new OpenLayers.Map(
											{
												div : "map_canvas",
												theme : null,
												resolutions : resolutionList,
												projection : new OpenLayers.Projection(
														"EPSG:23030"),
												displayProjection : new OpenLayers.Projection(
														"EPSG:4326"),
												maxExtent : new OpenLayers.Bounds(
														651500, 4590500,
														694100, 4645000),
												maxResolution : 53.125201382285255,
												controls : [
														new OpenLayers.Control.Navigation(),
														new OpenLayers.Control.ArgParser(),
														new OpenLayers.Control.Attribution() ]
											});

									// var gmap = new
									// OpenLayers.Layer.Google("Google");
									var markers = new OpenLayers.Layer.Markers(
											"Complaints");
									map.addLayer(markers);

									// WMS Estandar:
									// http://idezar.zaragoza.es/wms/IDEZar_base/IDEZar_base

									// WMS-C (Tileado):
									// http://idezar.zaragoza.es/IDEZar_Base_Tiled/WMSTileCache?&SERVICE=WMS&REQUEST=GetCapabilities

									// WMTS Estandar (Tileado)
									// http://idezar.zaragoza.es/IDEZar_Base_WMTS/TileCache

									var idezar = new OpenLayers.Layer.WMS(
											"IDEZar",
											"http://idezar.zaragoza.es/IDEZar_Base_Tiled/WMSTileCache",
											{
												version : '1.1.1',
												layers : 'base',
												format : 'image/png',
												transparent : false,
											},
											{
												projection : new OpenLayers.Projection(
														"EPSG:23030"),
												resolutions : [
														53.125201382285255,
														26.562600691142627,
														14.062553307075499,
														6.718775468936064,
														3.7500142152201517,
														1.7187565153092277,
														0.9375035538050343,
														0.5000018953626857,
														0.26375099980381617,
														0.131875499901908 ]
											});

									var tiles = "http://idezar.zaragoza.es/IDEZar_Base_Tiled/WMSTileCache";
									var sin = "http://idezar.zaragoza.es/wms/IDEZar_base/IDEZar_base";

									// var zaragoza = new
									// OpenLayers.Layer.WMS("Zaragoza Tiles",
									// sin, {
									// layers : "base",
									// transparent : "true",
									// format : "image/png"
									// }, {
									// isBaseLayer : true,
									// visibility : true,
									// singleTile : false
									// });

									// var osm = new OpenLayers.Layer.OSM();
									map.addLayers([ idezar ]);
									// map.addControl(new
									// OpenLayers.Control.LayerSwitcher());
									map
											.addControl(new OpenLayers.Control.MousePosition());
									// map.zoomToMaxExtent();
									map.setCenter(new OpenLayers.LonLat(676171,
											4613051), 2);
						
									/** RootScope variables */
									$rootScope.isLocationPickerMode = false;
									$rootScope.complaintLocation = null;
									$rootScope.newComplaintPhoto = null;
									$rootScope.selectedComplaint = null;

									/** Scope variables */
									$scope.requestList = [];

									/** Rest logging service */
									Restlogging
											.init("https://iescities.com:443");

									// Load public complaints
									complaintsService
											.getComplaints(function(data) {
												$scope.requestList = data;
												$rootScope.complaints = data;
												updateMarkers();
												Restlogging
														.appLog(
																logType.OD_CONSUME,
																"Zaragoza complaints accessed by user");
											});

									/** PUBLIC METHODS */
									// Gets map center
									$scope.getMapCenter = function() {
										var center = new OpenLayers.LonLat(
												map.center.lon, map.center.lat)
												.transform(
														new OpenLayers.Projection(
																"EPSG:23030"),
														new OpenLayers.Projection(
																"EPSG:4326"));
										return center;
									};

									// Resizes map according to window size
									$scope.resizeMap = function() {
										$(mapDiv).width($(window).width());
										$(mapDiv).height($(window).height());
									};

									// Close panel
									$scope.closePanel = function(panelId) {
										if (isWebKit()) {
											document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
										} else {
											document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
										}
										document.getElementById(panelId).style.zIndex = 999;
									};

									// Center map in position
									$scope.centerMapInPosition = function(pos) {
										document
												.getElementById("my_location_btn").className = "my_location_btn_normal ng-scope";
										document
												.getElementById("my_location_btn").disabled = false;
										var mapBounds = map.getExtent();
										var center = new OpenLayers.LonLat(
												pos.coords.longitude,
												pos.coords.latitude);
										center.transform(
												new OpenLayers.Projection(
														"EPSG:4326"), map
														.getProjectionObject());
										map.setCenter(center, 5);
										// if (mapBounds.containsLonLat(center))
										// {
										// // Center only if current position is
										// inside the map
										// map.setCenter(center, 5);
										// } else {
										// toastr.info("Su ubicacion actual esta
										// fuera del mapa de Zaragoza");
										// }
									};

									/** PRIVATE METHODS */
									function updateMarkers() {

										for ( var i in $scope.requestList) {
											var id = $scope.requestList[i].service_request_id;
											var lat = $scope.requestList[i].lat;
											var lon = $scope.requestList[i].long;

											var point = new OpenLayers.LonLat(
													lon, lat).transform(
													new OpenLayers.Projection(
															"EPSG:4326"),
													new OpenLayers.Projection(
															"EPSG:23030"));
											// alert (point.toShortString());
											/*
											 * var marker = marker = new
											 * OpenLayers.Marker(point,icon.clone());
											 * marker.events.register('mousedown',
											 * marker, function(evt) {
											 * alert("id:"+id);
											 * OpenLayers.Event.stop(evt); });
											 * markers.addMarker(marker);
											 */

											popupClass = OpenLayers.Popup.FramedCloud;

											if ($scope.requestList[i].media_url == null) {
												popupContentHTML = '<div id ="'
														+ $scope.requestList[i].service_request_id
														+ '" class="pop_theme">'
														+ '<span class="popup_title"> '
														+ $scope.requestList[i].title
														+ ' (ID: '
														+ $scope.requestList[i].service_request_id
														+ ')</span><br>'
														+ '<span class="popup_status"> Estado: '
														+ $scope.requestList[i].status
														+ '</span><br><br>'
														+ '<span>'
														+ $scope.requestList[i].description
														+ '</span><br>'
														+ '<br></div>';
											} else {
												// TODO Enrutar a otro partial
												popupContentHTML = '<div id ="'
														+ $scope.requestList[i].service_request_id
														+ '" class="pop_theme">'
														+ '<span class="popup_title"> '														
														+ $scope.requestList[i].title
														+ ' (ID: '
														+ $scope.requestList[i].service_request_id
														+ ')</span><br>'
														+ '<span class="popup_status"> Estado: '
														+ $scope.requestList[i].status
														+ '</span><br><br>'
														+ '<span>'
														+ $scope.requestList[i].description
														+ '</span><br>'
														+ '<br><a href="'
														+ $scope.requestList[i].media_url
														+ '">VER ADJUNTO</a><br></div>';
											}

											point.info = $scope.requestList[i];

											addMarker(
													point,
													popupClass,
													popupContentHTML,
													true,
													true,
													$scope.requestList[i].status);
										}
									}

									function addMarker(point, popupClass,
											popupContentHTML, closeBox,
											overflow, status) {

										var size = new OpenLayers.Size(32, 32);
										var offset = new OpenLayers.Pixel(
												-(size.w / 2), -size.h);

										var newIcon = new OpenLayers.Icon(
												'img/incident.png', size,
												offset);
										if (status == "closed") {
											newIcon = new OpenLayers.Icon(
													'img/success.png', size,
													offset);
										}

										var feature = new OpenLayers.Feature(
												markers, point);

										feature.closeBox = closeBox;
										feature.popupClass = popupClass;
										feature.data.popupContentHTML = popupContentHTML;
										feature.data.overflow = (overflow) ? "auto"
												: "hidden";
										feature.data.icon = newIcon;

										var marker = feature.createMarker();

										// var markerClick = function(evt) {
										// if (this.popup == null) {
										// this.popup = this
										// .createPopup(this.closeBox);
										// map.addPopup(this.popup);
										// this.popup.show();
										// } else {
										// this.popup.toggle();
										// }
										// currentPopup = this.popup;
										// OpenLayers.Event.stop(evt);
										// };

										var markerClick = function(evt) {
											$scope
													.$apply(function() {
														$rootScope.selectedComplaint = point;
													});
											OpenLayers.Event.stop(evt);
										};

										marker.events.register("mousedown",
												feature, markerClick);
										marker.events.register("touchstart",
												feature, markerClick);

										markers.addMarker(marker);
									}

									startRatingSurvey();									

								}
							}
						} ]);

/** Main menu directive */
iescities
		.directive(
				"iescitiesMenu",
				function($rootScope) {
					return function($scope) {

						// Close panel
						$scope.closePanel = function(panelId) {
							if (isWebKit()) {
								document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
							} else {
								document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
							}
							document.getElementById(panelId).style.zIndex = 999;
						};

						// Closes all panels
						$scope.closeAllPanels = function() {
							$scope.closePanel('publicComplaints');
							$scope.closePanel('userComplaints');
							$scope.closePanel('new_user');
							$scope.closePanel('my_profile');
							$scope.closePanel('login');
						};

						// Close all panels and unfade menu
						$scope.fade_back = function() {

							$scope.closeAllPanels();
							if (isWebKit()) {
								document.getElementById('main_menu').style.webkitTransform = "translate3d(0,0,0)";
							} else {
								document.getElementById('main_menu').style.left = '-14em';
							}
							document.getElementById('black_fader').style.zIndex = '0';
							document.getElementById('black_fader').style.opacity = '0';
						}

						// Toggles main menu
						$scope.toggle_main_menu = function() {
							if (document.getElementById('black_fader').style.zIndex == ""
									|| document.getElementById('black_fader').style.zIndex == "0") {
								// Mostrar menu
								if (isWebKit()) {
									document.getElementById('main_menu').style.webkitTransform = "translate3d(14em,0,0)";
								} else {
									document.getElementById('main_menu').style.left = '0';
								}

								document.getElementById('black_fader').style.zIndex = '990';
								document.getElementById('black_fader').style.opacity = '0.5';

							} else {
								// Ocultar menu
								if (isWebKit()) {
									document.getElementById('main_menu').style.webkitTransform = "translate3d(0,0,0)";
								} else {
									document.getElementById('main_menu').style.left = '-14em';
								}
								document.getElementById('black_fader').style.zIndex = '0';
								document.getElementById('black_fader').style.opacity = '0';
							}
						}

						$scope.showPanel = function(panelId) {
							$scope.fade_back();
							if (isWebKit()) {
								document.getElementById(panelId).style.webkitTransform = "translate3d(0,0,0)";
							} else {
								document.getElementById(panelId).style.transform = "translate3d(0,0,0)";
							}
							document.getElementById(panelId).style.zIndex = 200;
							hideKeyboard();
						};

						// ==================== IMAGE ENCODING
						// ==========================
						// ByteArrayOutputStream outputStream = new
						// ByteArrayOutputStream();
						// int data;
						// while ((data = inputStream.read()) >= 0) {
						// outputStream.write(data);
						// }
						// inputStream.close();
						//
						// Base64 encoder = new Base64();
						// String retorno = new
						// String(encoder.encode(outputStream.toByteArray()));
						// return URLEncoder.encode(retorno);
						// ==================== IMAGE ENCODING
						// ==========================
					};
				});
